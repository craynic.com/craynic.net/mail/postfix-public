#!/usr/bin/env bash

set -Eeuo pipefail

POSTFIX_SERVICE_NAME="smtp-amavis"
POSTFIX_SERVICE_MAXPROC="10"

if [[ -z "$POSTFIX_AMAVIS_ADDRESS" ]]; then
  exit 0
fi

if ! [[ "$POSTFIX_AMAVIS_ADDRESS" =~ ^\[.*\]:[[:digit:]]+$ ]]; then
  echo "The Amavis address does not use the square brackets notation [server]:port." \
    "That results in MX lookups on the server address. Are you sure you want that?"
fi

update-postfix-config.sh <(echo "content_filter = $POSTFIX_SERVICE_NAME:$POSTFIX_AMAVIS_ADDRESS")

# add the service
postconf -M "$POSTFIX_SERVICE_NAME/unix=smtp-amavis unix - - n - $POSTFIX_SERVICE_MAXPROC smtp"
# enable XFORWARD
postconf -P "$POSTFIX_SERVICE_NAME/unix/smtp_send_xforward_command=yes"
# make TLS encryption mandatory
postconf -P "$POSTFIX_SERVICE_NAME/unix/smtp_tls_security_level=encrypt"
