#!/usr/bin/env bash

set -Eeuo pipefail

# run all init scripts
run-parts --exit-on-error --regex='\.sh$' -- "/docker-init.d"

ENVS=(
  POSTFIX_HOSTNAME
  POSTFIX_MYSQL_HOST
  POSTFIX_MYSQL_USER
  POSTFIX_MYSQL_PASS
  POSTFIX_MYSQL_DB
  POSTFIX_SMTPD_MILTERS
  POSTFIX_RELAY_TRANSPORT
  POSTFIX_POSTSRS_ADDRESS
  POSTFIX_POLICY_SERVICE_ADDRESS
  POSTFIX_HAPROXY
  POSTFIX_MESSAGE_SIZE_LIMIT
)

# cleanup ENV variables
for e in "${ENVS[@]}"; do
  unset "$e"
done

# run
exec "$@"
